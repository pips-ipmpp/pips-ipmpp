//
// Created by nils-christian on 27.05.21.
//

#include "DistributedMatrixUtilities.h"

SparseMatrix* getSparseGenMatrixFromStochMat(const DistributedMatrix& mat, int node, BlockType block) {
   assert(-1 <= node && node < static_cast<int>(mat.children.size()));

   if (node == -1) {
      if (block == BlockType::BL_MAT) {
         assert(mat.Blmat->is_a(kSparseGenMatrix));
         return dynamic_cast<SparseMatrix*>(mat.Blmat.get());
      } else {
         assert(block == BlockType::B_MAT);
         assert(mat.Bmat->is_a(kSparseGenMatrix));
         return dynamic_cast<SparseMatrix*>(mat.Bmat.get());
      }
   } else {
      if (block == BlockType::A_MAT) {
         assert(mat.children[node]->Amat->is_a(kSparseGenMatrix));
         return dynamic_cast<SparseMatrix*>(mat.children[node]->Amat.get());
      } else if (block == BlockType::B_MAT) {
         assert(mat.children[node]->Bmat->is_a(kSparseGenMatrix));
         return dynamic_cast<SparseMatrix*>(mat.children[node]->Bmat.get());
      } else if (block == BlockType::BL_MAT) {
         assert(mat.children[node]->Blmat->is_a(kSparseGenMatrix));
         return dynamic_cast<SparseMatrix*>(mat.children[node]->Blmat.get());
      }
   }
   return nullptr;
}

SparseSymmetricMatrix& getSparseSymmetricDiagFromStochMat(const DistributedSymmetricMatrix& mat, int node)
{
   if (node == -1) {
      assert(mat.diag);
      assert(!mat.border);
      return dynamic_cast<SparseSymmetricMatrix&>(*mat.diag);
   } else {
      assert(0 <= node && node <= static_cast<int>(mat.children.size()));
      assert(mat.children[node]);

      assert(mat.children[node]->diag);
      return dynamic_cast<SparseSymmetricMatrix&>(*mat.children[node]->diag);
   }
}


SparseMatrix& getSparseBorderFromStochMat(const DistributedSymmetricMatrix& mat, int node)
{
   assert(0 <= node && node <= static_cast<int>(mat.children.size()));
   assert(mat.children[node]);

   assert(mat.children[node]->border);
   return dynamic_cast<SparseMatrix&>(*mat.children[node]->border);
}
