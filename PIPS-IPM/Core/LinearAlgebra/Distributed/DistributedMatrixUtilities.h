/*
 * StochMatrixUtilities.h
 *
 *  Created on: 27.11.2019
 *      Author: bzfkempk
 */
#ifndef DISTRIBUTEDMATRIXUTILITIES_H
#define DISTRIBUTEDMATRIXUTILITIES_H

#include "SparseMatrix.h"
#include "DistributedMatrix.h"
#include "../Preprocessing/SystemType.hpp"

#include <vector>

SparseMatrix* getSparseGenMatrixFromStochMat(const DistributedMatrix& mat, int node, BlockType block);
SparseSymmetricMatrix& getSparseSymmetricDiagFromStochMat(const DistributedSymmetricMatrix& mat, int node);
SparseMatrix& getSparseBorderFromStochMat(const DistributedSymmetricMatrix& mat, int node);

#endif /* DISTRIBUTEDMATRIXUTILITIES_H */
