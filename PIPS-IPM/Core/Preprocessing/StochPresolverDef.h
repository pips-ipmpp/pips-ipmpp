#pragma once

/* access to row data */
#define presol_get_ineq_lhs(row) (getSimpleVecFromRowStochVec(*((presProb)->inequality_lower_bounds), (row)))
#define presol_get_ineq_rhs(row) (getSimpleVecFromRowStochVec(*((presProb)->inequality_upper_bounds), (row)))

#define presol_get_ineq_lhs_ind(row) (getSimpleVecFromRowStochVec(*((presProb)->inequality_lower_bound_indicators), (row)))
#define presol_get_ineq_rhs_ind(row) (getSimpleVecFromRowStochVec(*((presProb)->inequality_upper_bound_indicators), (row)))
#define presol_has_ineq_lhs(row) (getSimpleVecFromRowStochVec(*((presProb)->inequality_lower_bound_indicators), (row)))
#define presol_has_ineq_rhs(row) (getSimpleVecFromRowStochVec(*((presProb)->inequality_upper_bound_indicators), (row)))

#define presol_get_eq_rhs(row) (getSimpleVecFromRowStochVec(*((presProb)->equality_rhs), (row)))

#define presol_has_row_lhs(row) ((row).is_equality() ? true : presol_has_ineq_lhs(row))
#define presol_has_row_rhs(row) ((row).is_equality() ? true : presol_has_ineq_rhs(row))
#define presol_get_row_lhs(row) ((row).is_equality() ? presol_get_eq_rhs(row) : presol_get_ineq_lhs(row))
#define presol_get_row_rhs(row) ((row).is_equality() ? presol_get_eq_rhs(row) : presol_get_ineq_rhs(row))

#define presol_get_eq_nnz(row) (getSimpleVecFromRowStochVec(*(nnzs_row_A), (row)))
#define presol_get_ineq_nnz(row) (getSimpleVecFromRowStochVec(*(nnzs_row_C), (row)))

/* the non-zeros of a row might be outdated; changes in linking rows are cached in nnzs_row_A_chgs and need to be reduced */
#define presol_get_row_nnz(row) ((row).is_equality() ? presol_get_eq_nnz(row) : presol_get_ineq_nnz(row))
#define presol_get_row_nnz_changes(row) ((row).is_linking_row() ? ((row).is_equality() ? (*(nnzs_row_A_chgs))[(row).index()] : (*(nnzs_row_C_chgs))[(row).index()]) : 0)

/* access to column data */
#define presol_get_column_objective(col) (getSimpleVecFromColStochVec(*((presProb)->objective_gradient), (col)))
#define presol_get_column_lb(col) (getSimpleVecFromColStochVec(*((presProb)->primal_lower_bounds), (col)))
#define presol_get_column_ub(col) (getSimpleVecFromColStochVec(*((presProb)->primal_upper_bounds), (col)))

#define presol_has_column_lb(col) (getSimpleVecFromColStochVec(*((presProb)->primal_lower_bound_indicators), (col)))
#define presol_has_column_ub(col) (getSimpleVecFromColStochVec(*((presProb)->primal_upper_bound_indicators), (col)))

/* the non-zeros of a column might be outdated; changes in linking columns are cached in nnzs_col_chgs and need to be reduced */
#define presol_get_column_nnz(col) (getSimpleVecFromColStochVec(*nnzs_col, (col)))
#define presol_get_column_nnz_changes(col) ((col).is_linking_col() ? (*(nnzs_col_chgs))[(col).index()] : 0)
