/*
 * SystemType.h
 *
 *  Created on: 02.05.2019
 *      Author: bzfkempk
 */
#pragma once

#include <ostream>

enum class SystemType
{
   EQUALITY_SYSTEM,
   INEQUALITY_SYSTEM
};

enum class BlockType
{
   A_MAT,
   B_MAT,
   BL_MAT
};
