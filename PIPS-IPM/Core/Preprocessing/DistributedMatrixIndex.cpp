#include "DistributedMatrixIndex.hpp"

std::ostream &operator<<(std::ostream &out, const SystemType &sys)
{
   switch (sys)
   {
   case SystemType::EQUALITY_SYSTEM:
      out << "EQUA_SYS";
      break;
   case SystemType::INEQUALITY_SYSTEM:
      out << "INEQ_SYS";
      break;
   }
   return out;
}

std::ostream &operator<<(std::ostream &out, const BlockType &block)
{
   switch (block)
   {
   case BlockType::A_MAT:
      out << "A_MAT";
      break;
   case BlockType::B_MAT:
      out << "B_MAT";
      break;
   case BlockType::BL_MAT:
      out << "BL_MAT";
      break;
   }
   return out;
}
