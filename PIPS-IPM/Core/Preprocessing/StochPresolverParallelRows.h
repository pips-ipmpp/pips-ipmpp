/*
 * StochPresolverParallelRows.h
 *
 *  Created on: 02.05.2018
 *      Author: bzfuslus
 */

#ifndef PIPS_IPM_CORE_QPPREPROCESS_STOCHPRESOLVERPARALLELROWS_H_
#define PIPS_IPM_CORE_QPPREPROCESS_STOCHPRESOLVERPARALLELROWS_H_

#include "Hashing.h"
#include "StochPresolverBase.h"

#include <functional>
#include <unordered_set>

namespace rowlib {
   struct rowData {
      const int id;
      int supportid;
      const int offset_nA;
      const int lengthA;
      const int* const colIndicesA;
      const double* const entriesA;
      const int lengthB;
      const int* const colIndicesB;
      const double* const entriesB;

      rowData(int id, int offset_nA, int lengthA, const int* const colIndicesA,
            const double* const entriesA, const int lengthB, const int* const colIndicesB,
            const double* const entriesB)
         :  id(id),
            supportid(id),
            offset_nA(offset_nA),
            lengthA(lengthA),
            colIndicesA(colIndicesA),
            entriesA(entriesA),
            lengthB(lengthB),
            colIndicesB(colIndicesB),
            entriesB(entriesB)
      {
      }

      friend std::ostream& operator<<(std::ostream& out, const rowData& row) {
         out << "ID: " << row.id << ",offset_nA: " << row.offset_nA << ", A_col[";
         for (int i = 0; i < row.lengthA; ++i) {
            if (i != 0)
               out << ",";
            out << row.colIndicesA[i];
         }
         out << "], B_col[";
         for (int i = 0; i < row.lengthB; ++i) {
            if (i != 0)
               out << ",";
            out << row.colIndicesB[i];
         }
         out << "]";
         return out;
      }

      size_t hash_value() const {
         HashUtils hash(lengthA + lengthB);
         for (int i = 0; i < lengthA; i++) {
            hash.combine(colIndicesA[i]);
         }
         for(int i = 0; i < lengthB; i++) {
            hash.combine(colIndicesB[i] + offset_nA);
         }

         return hash.getHash();
      }

      size_t coeff_hash_value() const {
         HashUtils hash(lengthA + lengthB);

         for (int i = 0; i < lengthA; i++) {
            hash.combine(entriesA[i]);
         }
         for(int i = 0; i < lengthB; i++) {
            hash.combine(entriesB[i]);
         }

         return hash.getHash();
      }

      /* the comparison operator is used to determine if two rows have the same column indices */
      bool operator==(rowData const& other) const {
         if (lengthA != other.lengthA)
            return false;

         if (lengthB != other.lengthB)
            return false;

         if (memcmp(static_cast<const void*>(colIndicesA),
               static_cast<const void*>(other.colIndicesA),
               lengthA * sizeof(int)))
            return false;

         return !memcmp(static_cast<const void*>(colIndicesB),
               static_cast<const void*>(other.colIndicesB),
               lengthB * sizeof(int));
      }
   };

   struct supportHash {
      size_t operator()(const rowData& row) const {
         return row.hash_value();
      }
   };

   struct supportEqual {
      bool operator()(const rowData& row1, const rowData& row2) const {
         return (row1 == row2);
      }
   };
}

class StochPresolverParallelRows : public StochPresolverBase {
public:
   using row_hashtable = std::unordered_map<rowlib::rowData, int, rowlib::supportHash, rowlib::supportEqual>;

   StochPresolverParallelRows(PresolveData& presolve_data, const DistributedProblem& origProb);

   ~StochPresolverParallelRows();

   // remove parallel rows
   bool applyPresolving() override;

private:

   /** tolerance for comparing two double values in two different rows and for them being considered equal */
   const double limit_tol_compare_entries;
   int n_rows_removed{0};

   /// extension to the pointer set from StochPresolverBase to point to C and A at the same moment rather than
   /// distinguishing between EQUALITY and INEQUALITY constraints
   const SparseStorageDynamic* currCmat{};
   const SparseStorageDynamic* currCmatTrans{};
   const SparseStorageDynamic* currDmat{};
   const SparseStorageDynamic* currDmatTrans{};
   const DenseVector<int>* currNnzRowC{};

   // pointers to the normalized and copied matrix blocks
   std::unique_ptr<SparseStorageDynamic> norm_Amat{};
   std::unique_ptr<SparseStorageDynamic> norm_Bmat{};
   std::unique_ptr<SparseStorageDynamic> norm_Cmat{};
   std::unique_ptr<SparseStorageDynamic> norm_Dmat{};
   std::unique_ptr<DenseVector<double>> norm_b{};
   std::unique_ptr<DenseVector<double>> norm_clow{};
   std::unique_ptr<DenseVector<double>> norm_cupp{};
   std::unique_ptr<DenseVector<double>> norm_iclow{};
   std::unique_ptr<DenseVector<double>> norm_icupp{};
   std::unique_ptr<DenseVector<double>> norm_factorC{};
   std::unique_ptr<DenseVector<double>> norm_factorA{};

   // data for the nearly parallel row case
   std::unique_ptr<DenseVector<int>> rowContainsSingletonVariableA{};
   std::unique_ptr<DenseVector<int>> rowContainsSingletonVariableC{};
   std::unique_ptr<DenseVector<double>> singletonCoeffsColParent{};
   std::unique_ptr<DenseVector<double>> singletonCoeffsColChild{};
   std::unique_ptr<DenseVector<int>> normNnzRowA{};
   std::unique_ptr<DenseVector<int>> normNnzRowC{};
   std::unique_ptr<DenseVector<int>> normNnzColParent{};
   std::unique_ptr<DenseVector<int>> normNnzColChild{};
   std::unique_ptr<SparseStorageDynamic> norm_AmatTrans{};
   std::unique_ptr<SparseStorageDynamic> norm_BmatTrans{};
   std::unique_ptr<SparseStorageDynamic> norm_CmatTrans{};
   std::unique_ptr<SparseStorageDynamic> norm_DmatTrans{};

   // number of rows of the A or B block
   int mA{0};
   // number of columns of the A or C block
   int nA{0};

   void setNormalizedPointers(int node);
   void setNormalizedPointersMatrices(int node);
   void setNormalizedPointersMatrixBounds(int node);
   void setNormalizedNormFactors(int node);
   void setNormalizedSingletonFlags(int node);
   void setNormalizedReductionPointers(int node);
   void updateExtendedPointersForCurrentNode(int node);

   void removeSingletonVars();
   void removeEntry(int colIdx, DenseVector<int>& rowContainsSingletonVar, SparseStorageDynamic& matrix, SparseStorageDynamic& matrixTrans,
         DenseVector<int>& nnzRow, DenseVector<int>& nnzCol, bool parent);
   double removeEntryInDynamicStorage(SparseStorageDynamic& storage, int row, int col) const;

   void normalizeBlocksRowwise(SystemType system_type, SparseStorageDynamic* a_mat, SparseStorageDynamic* b_mat, DenseVector<double>* cupp,
         DenseVector<double>* clow, DenseVector<double>* icupp, DenseVector<double>* iclow) const;
   void insertRowsIntoHashtable(row_hashtable& rows, std::vector<rowlib::rowData>& row_data,
      std::vector<int>& row_index, const SparseStorageDynamic* Ablock,
      const SparseStorageDynamic* Bblock, SystemType system_type, const DenseVector<int>* nnz_row_norm,
      const DenseVector<int>* nnz_row_orig);
   size_t getBucketEnd(const std::vector<int>& row_index, const std::vector<rowlib::rowData>& row_data, size_t start);
   void compareRowsInBucket(size_t start, size_t end, const std::vector<int>& row_index,
      const std::vector<rowlib::rowData>& row_data, int& nRowElims, int node);
   bool checkRowsAreParallel(const rowlib::rowData& row1, const rowlib::rowData& row2) const;

   void tightenOriginalBoundsOfRow1(const ROW_INDEX& row1, const ROW_INDEX& row2) const;

   double getSingletonCoefficient(const COL_INDEX& col) const;

   COL_INDEX getRowSingletonVariable(const ROW_INDEX& row) const;
   bool rowContainsSingletonVariable(const ROW_INDEX& row) const;

   void tightenBoundsForSingleVar(int singleColIdx, double newxlow, double newxupp);

   bool twoParallelEqualityRows(const ROW_INDEX& row1, const ROW_INDEX& row2) const;
   bool parallelEqualityAndInequalityRow(const ROW_INDEX& row_eq, const ROW_INDEX& row_ineq) const;
   bool twoParallelInequalityRows(const ROW_INDEX& row1, const ROW_INDEX& row2) const;

   bool twoNearlyParallelEqualityRows(const ROW_INDEX& row1, const ROW_INDEX& row2) const;
   bool nearlyParallelEqualityAndInequalityRow(const ROW_INDEX& row_eq, const ROW_INDEX& row_ineq) const;
   bool twoNearlyParallelInequalityRows(const ROW_INDEX& row1, const ROW_INDEX& row2) const;

   void tightenLinkingVarsBounds();

};

#endif /* PIPS_IPM_CORE_QPPREPROCESS_STOCHPRESOLVERPARALLELROWS_H_ */
