/*
 * Hashing.h
 *
 *  Created on: 10.06.2024
 *      Author: Stephen J. Maher
 */

#include <cmath>
#include <cstdint>

struct HashUtils {
   size_t state;
   const std::uint64_t multiplier = 0x9e3779b97f4a7c15;
   const double golden_ratio = 0.61803398874989484;
   const double mantissa_factor = 10000;

   HashUtils(size_t init = 0) : state(init) {}

   // computes a hash code for a double and returns this as size_t
   size_t hashCode(double value) {
      // first multiply the value by an irrational number. In this case we use the golden ratio.
      int value_to_hash;
      double mantissa = std::frexp(value * golden_ratio, &value_to_hash);

      // TODO: this is taken from the original implementation. Could look at other ways to compute the value to hash,
      // such as using casting and bit shifting. However, this could be covered in the combine method.
      value_to_hash += 10 * ((int) std::trunc(mantissa * mantissa_factor));

      return value_to_hash;
   }

   void combine(size_t value) {
      state += multiplier + size_t(value);
      state ^= state >> 32;
      state *= multiplier;
      state ^= state >> 32;
      state *= multiplier;
      state ^= state >> 28;
   }

   void combine(int value) {
      return combine((size_t)value);
   }

   void combine(double value) {
       return combine(hashCode(value));
   }

   size_t getHash() const {
      return state;
   }
};
