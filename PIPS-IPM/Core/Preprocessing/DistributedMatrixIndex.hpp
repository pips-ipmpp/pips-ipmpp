#pragma once

#include "SystemType.hpp"
#include "pipsdef.h"

std::ostream &operator<<(std::ostream &out, const SystemType &system);
std::ostream &operator<<(std::ostream &out, const BlockType &block);

/* index describing a specific row in the problem */
struct ROW_INDEX
{
public:
   ROW_INDEX(SystemType system, int node, int index, bool linking) : system_(system), node_(node), index_(index), linking_(linking)
   {
      assert(node >= -1);
      assert(index >= -1);
      assert_if_then(linking, node == -1);
      assert_if_then(index == -1, node == -1 && is_empty());
   }

   ROW_INDEX() : system_(SystemType::EQUALITY_SYSTEM), node_(-1), index_(-1), linking_(false) {};

   // TODO: necessary to distinguish between empty EQ/INEQ rows?
   [[nodiscard]] static ROW_INDEX EMPTY_ROW()
   {
      return ROW_INDEX();
   }

   bool operator==(const ROW_INDEX &row) const
   {
      return node_ == row.node_ && index_ == row.index_ && linking_ == row.linking_ && system_ == row.system_;
   }

   bool operator!=(const ROW_INDEX &row) const
   {
      return node_ != row.node_ || index_ != row.index_ || linking_ != row.linking_ || system_ != row.system_;
   }

   [[nodiscard]] inline int node() const { return node_; };
   [[nodiscard]] inline int index() const { return index_; };
   [[nodiscard]] inline SystemType system_type() const { return system_; };
   [[nodiscard]] inline bool is_linking_row() const { return linking_; }
   [[nodiscard]] inline bool is_node_valid(int nChildren) const { return -1 <= node_ && node_ < nChildren; };
   [[nodiscard]] inline bool is_equality() const { return system_ == SystemType::EQUALITY_SYSTEM; }
   [[nodiscard]] inline bool is_inequality() const { return system_ == SystemType::INEQUALITY_SYSTEM; }
   [[nodiscard]] inline bool is_empty() const { return *this == EMPTY_ROW(); };

   friend std::ostream &operator<<(std::ostream &out, const ROW_INDEX &row)
   {
      out << "ROW(" << row.system_ << "," << row.node_ << "," << row.index_ << "," << (row.linking_ ? "linking)" : "no-link)");
      return out;
   }

private:
   SystemType system_;
   int node_;
   int index_;
   bool linking_;
};

/* index describing a specific column in the problem */
struct COL_INDEX
{
public:
   COL_INDEX(int node, int index) : node_(node), index_(index)
   {
      assert(node >= -1);
      assert(index >= -1);
      assert_if_then(index == -1, node == -1 && is_empty());
   }

   COL_INDEX() : node_(-1), index_(-1) {};

   [[nodiscard]] static COL_INDEX EMPTY_COL()
   {
      return COL_INDEX();
   }

   bool operator==(const COL_INDEX &col) const
   {
      return node_ == col.node_ && index_ == col.index_;
   }

   bool operator!=(const COL_INDEX &col) const
   {
      return node_ != col.node_ || index_ != col.index_;
   }

   [[nodiscard]] inline int node() const { return node_; };
   [[nodiscard]] inline int index() const { return index_; };
   [[nodiscard]] inline bool is_linking_col() const { return node_ == -1; }
   [[nodiscard]] inline bool is_node_valid(int nChildren) const { return -1 <= node_ && node_ < nChildren; };
   [[nodiscard]] inline bool is_empty() const { return *this == EMPTY_COL(); }

   friend std::ostream &operator<<(std::ostream &out, const COL_INDEX &col)
   {
      out << "COL(" << col.node_ << "," << col.index_ << ")";
      return out;
   }

private:
   int node_;
   int index_;
};

/* index describing a specific sub-matrix in the problem */
class BLOCK_INDEX
{

   // TODO: internally A0 is not A0 but Bmat B0..
   /* Internally, matrices are defined as
    *
    * B-1
    * A0   B0
    * A1        B1
    * .             .
    * .                .
    * .                   .
    * AN-1                     BN-1
    * BL_-1 BL_0 BL_1  . . . . BL_N-1
    *
    * where indices are called the nodes of the matrix, ranging from -1 to N. Often within pips, we refer to these matrices as
    *
    * B0
    * A1   B1
    * A2        B2
    * .             .
    * .                .
    * .                   .
    * AN1                      BN
    * BL_0 BL_1 BL_2  . . . . BL_N
    *
    */

public:
   BLOCK_INDEX(SystemType system, BlockType block, int node) : system_(system), block_(block), node_(node)
   {
      // TODO: store B0_MAT as A0_MAT internally..
      assert_if_then(node == -1, block == BlockType::B_MAT || block == BlockType::BL_MAT);
   }

   bool operator==(const BLOCK_INDEX &block) const
   {
      return system_ == block.system_ && block_ == block.block_ && node_ == block.node_;
   }

   bool operator!=(const BLOCK_INDEX &block) const
   {
      return node_ != block.node_ || block_ != block.block_ || node_ != block.node_;
   }

   [[nodiscard]] inline SystemType system_type() const { return system_; };
   [[nodiscard]] inline BlockType block_type() const { return block_; };
   [[nodiscard]] inline int node() const { return node_; };
   [[nodiscard]] inline bool is_node_valid(int nChildren) const { return -1 <= node_ && node_ < nChildren; };

   /* return the unique BLOCK_INDEX in which row and column intersect */
   [[nodiscard]] static BLOCK_INDEX get_block_index_from_row_col(const ROW_INDEX &row, const COL_INDEX &col)
   {
      BlockType block = row.is_linking_row() ? BlockType::BL_MAT : ((col.is_linking_col() && row.node() != -1) ? BlockType::A_MAT : BlockType::B_MAT);

      return BLOCK_INDEX(row.system_type(), block, row.is_linking_row() ? col.node() : row.node());
   }

   friend std::ostream &operator<<(std::ostream &out, const BLOCK_INDEX &block)
   {
      out << "Block(" << block.system_ << "," << block.node_ << "," << block.block_ << ")";
      return out;
   }

private:
   SystemType system_;
   BlockType block_;
   int node_;
};
