/*
 * StochPresolverSingletonColumns.h
 *
 *  Created on: 08.05.2018
 *      Author: bzfuslus
 */

#ifndef PIPS_IPM_CORE_QPPREPROCESS_STOCHPRESOLVERSINGLETONCOLUMNS_H_
#define PIPS_IPM_CORE_QPPREPROCESS_STOCHPRESOLVERSINGLETONCOLUMNS_H_

#include "StochPresolverBase.h"
#include <limits>


class StochPresolverSingletonColumns : public StochPresolverBase {
public:
   StochPresolverSingletonColumns(PresolveData& presolve_data, const DistributedProblem& origProb);

   ~StochPresolverSingletonColumns() override = default;

   bool applyPresolving() override;

private:
   long long removed_cols;
   bool local_singletons;
   const unsigned int n_linking_rows_eq;
   const unsigned int n_linking_rows_ineq;

   std::vector<int> local_linking_column_for_row_in_proc;
   std::vector<COL_INDEX> cols;
   std::vector<double> coeffs;


   bool removeSingletonColumn(const COL_INDEX& col);
   ROW_INDEX findRowForColumnSingleton(const COL_INDEX& col, bool& found);
   ROW_INDEX findRowForLinkingSingleton(int col, bool& found);
   ROW_INDEX findRowForLinkingSingletonInSystem(int col, SystemType system_type, bool& found);
   ROW_INDEX findRowForNonlinkingSingelton(const COL_INDEX& col, bool& found);
   bool findRowForSingletonColumnInMatrix(const SparseStorageDynamic& mat, int& row, const int& col);

   void checkColImpliedFree(const COL_INDEX& col, const ROW_INDEX& row, bool& lb_implied_free, bool& ub_implied_free);
   void resetArrays();
};


#endif /* PIPS_IPM_CORE_QPPREPROCESS_STOCHPRESOLVERSINGLETONCOLUMNS_H_ */
